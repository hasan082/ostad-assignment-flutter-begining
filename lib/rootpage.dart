import 'package:assignment_flutter_beginning/pages/page1.dart';
import 'package:assignment_flutter_beginning/pages/page2.dart';
import 'package:assignment_flutter_beginning/pages/page3.dart';
import 'package:assignment_flutter_beginning/widgets/bottom_nav_widget.dart';
import 'package:assignment_flutter_beginning/widgets/drawer_widget.dart';
import 'package:flutter/material.dart';

class RootPage extends StatefulWidget {
  final int? feedIndex;
  const RootPage({Key? key, this.feedIndex}) : super(key: key);

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  late int _selectedIndex;

  @override
  void initState() {
    super.initState();
    _selectedIndex = (widget.feedIndex ?? 0);
  }

  static const List<Widget> _pages = [
    PageOne(),
    PageTwo(),
    PageThree(),
  ];

  static const List<String> _pageTitles = [
    'Page 1',
    'Page 2',
    'Page 3',
  ];


  void _onIndexChanged(int newIndex) {
    setState(() {
      _selectedIndex = newIndex;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pageTitles[_selectedIndex]),
      ),
      drawer: const CustomDrawer(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: Colors.green,
        shape: const CircleBorder(),
        onPressed: () {
        },
        child: const Icon(Icons.add),
      ),
      body: IndexedStack(
        index: _selectedIndex,
        children: _pages,
      ),
      bottomNavigationBar: BottomWidget(
        currentIndex: _selectedIndex,
        onIndexChanged: _onIndexChanged,
      ),
    );
  }
}
