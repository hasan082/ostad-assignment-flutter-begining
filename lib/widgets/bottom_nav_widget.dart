import 'package:flutter/material.dart';

class BottomWidget extends StatelessWidget {
  final int currentIndex;
  final ValueChanged<int> onIndexChanged;

  const BottomWidget({
    Key? key,
    required this.currentIndex,
    required this.onIndexChanged,
  }) : super(key: key);

  static const List<String> _pageTitles = [
    'Home',
    'Favorite',
    'Profile',
  ];


  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      selectedFontSize: 14,
      selectedItemColor: Colors.green,
      unselectedItemColor: Colors.grey,
      unselectedFontSize: 14,
      currentIndex: currentIndex,
      onTap: onIndexChanged,
      items: [
        BottomNavigationBarItem(
          icon: const Icon(Icons.home),
          label: _pageTitles[0],
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.favorite),
          label: _pageTitles[1],
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.person),
          label: _pageTitles[2],
        ),
      ],
    );
  }
}
