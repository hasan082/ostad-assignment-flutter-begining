import 'package:assignment_flutter_beginning/rootpage.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final drawerWidth = screenWidth < 600 ? 300.0 : screenWidth;
    return Drawer(
      width: drawerWidth,
      child: ListView(
        children: [
          const UserAccountsDrawerHeader(
            accountName: Text('Your Name Here'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage('https://picsum.photos/200'),
            ),
            accountEmail: Text('User Email Here'),
            decoration: BoxDecoration(
              color: Colors.green,
            ),
          ),
          buildDrawerItem(
            title: 'Page 1',
            icon: Icons.arrow_forward_rounded,
            onTap: () {
              navigateToPage(context, 0);
            },
          ),
          buildDrawerItem(
            title: 'Page 2',
            icon: Icons.arrow_forward_rounded,
            onTap: () {
              navigateToPage(context, 1);
            },
          ),
          buildDrawerItem(
            title: 'Page 3',
            icon: Icons.arrow_forward_rounded,
            onTap: () {
              navigateToPage(context,  2);
            },
          ),
        ],
      ),
    );
  }

  Widget buildDrawerItem({
    required String title,
    required IconData icon,
    required VoidCallback onTap,
  }) {
    return ListTile(
      title: Text(title),
      trailing: Icon(icon),
      onTap: onTap,
    );
  }

  void navigateToPage(BuildContext context, int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
        RootPage(
            feedIndex: index),
      ),
    );

  }
}

